# Shortest Path Solver
## Dijkstra's Algorithm

### Achievements:
* Implemented Shortest Path Function in Scala
* Implemented Dijkstra's Algorithm in Scala
* Implemented Priority Queue in Scala
* Utilized Text Files to Represent Weighted Digraphs
* Created Test Suites using ScalaTest Framework
* Managed Source Builds using Scala Build Tool
* Cleaned Data for Performance Testing

#### Design:
* Shortest Path Function
    - Uses Dijkstra'a Algorithm to build a shortest path from source vertex
      to destination vertex based on provided edge weights, if path exists
* Dijkstra's Algorithm
    - Scala implementation utilizes an indexed min-heap data structure to
      reduce time complexity from O(V^2) if used unsorted list to
      O(E log V), where V is the number of vertices and E is the number of
      edges in graph G
* Weighted Digraph
    - For each graph (G), a text file represents G's directed edges (E),
      from which G's vertices (V) are built
    - The provided set of inputs was transformed from a list of undirected
      edges to one of directed edges, since it is possible to represent
      an undirected graph in directed format, but not the other way around
    - Cleaned test data from [Princeton](http://algs4.cs.princeton.edu/44sp/) to match expected
      format of comma separated values

### Get It!
    git clone git@github.com:baldwmic/dijkstras.git

### Run It!
    brew install scala sbt
    sbt run

### Test It!
    sbt test

### Stress Test It!
    // test files for performance testing
    // url: 'https://algs4.cs.princeton.edu/44sp/'
    // files: ['tinyEWD.txt', 'mediumEWD.txt', '1000EWD.txt', '10000EWD.txt', 'largeEWD.txt']
    // 
    // download test file
    wget -P src/test/resources/performance https://algs4.cs.princeton.edu/44sp/tinyEWD.txt
    // 
    // clean test file
    python src/test/resources/cleanData/cleanTestFile.py src/test/resources/performance/tinyEWD.txt
    //
    // change "ignore" to "test" in 'src/test/scala/PerformanceSuite.scala'
    // 
    // run test
    sbt test

#### About Testing
* Solution Tests
    - High-level tests for the solution are provided in the ShortestPathSolver
          test suite.
* Ignored Tests
    - The tests in the Performance test suite are ignored due to the long
          wait time running these tests incurs. The test files are omitted from the repository but can be [downloaded here](http://algs4.cs.princeton.edu/44sp/).

##### Example Test

    test("ShortestPathSolver should find shortest path from " + A + " to " + C) {
        d.fileName = strDir + "fileName.txt"
        val result = d.shortestPath(A, C, d.readFile)
        assert(result("distance") === "10.0")
        val path = A + " => " + B + " => " + C
        assert(result("path") === path)
    }
