// factor out common settings into a sequence
lazy val commonSettings = Seq(
  organization := "michaelbaldwin.shortestpathsolver",
  version := "1.0",
  // set the Scala version used for the project
  scalaVersion := "2.11.6"
)

// add scalatest framework to dependencies
libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    // set the name of the project
    name := "ShortestPathSolver"
  )
