#
# @project Shortest Path Solver
# @author Michael Baldwin
# @date 06/15/2015
# @usage Clean text files for performance testing.
# @description
#
# Python script to clean text files for use in testing implementation of
# Dijkstra's algorithm.
#
# Original files are separated by spaces, but final resulting files should be
# comma separated values.
#
# @references Test Data: http://algs4.cs.princeton.edu/44sp/
#

# import os, sys, shutil, fileinput libraries
import os, sys, shutil, fileinput

# unpack values passed as arguments and assign to variables
script, fileName = sys.argv

# if file exists clean it, else warn user
if os.path.exists(fileName):

    # create backup directory name
    backupDir = os.path.join(os.getcwd(), '.backup')

    # if does not already exist, create backup directory
    if not os.path.exists(backupDir):
        os.makedirs(backupDir)

    print 'Backing up %r file to %r.' % (fileName, backupDir)

    # copy file to backup directory
    shutil.copy(fileName, backupDir)

    print 'Cleaning %r file.' % fileName

    # open file and direct standard output to input file to write to it
    index = 0
    for line in fileinput.input(fileName, inplace=1):

        # if line is first or second, ignore
        if index == 0 or index == 1:
          index += 1
          continue

        # create string variable for new line
        newLine = ''

        # split line string into list of items, whitespace as delimiter
        items = line.split()

        # for each item in list
        for item in items:

            # if item is last item in list do not add comma, else add comma
            if item == items[-1]:
                newLine += str(item)
            else:
                newLine += str(item) + ', '

        # print the new line which overwrites current line
        print newLine

    # now back to printing standard output to console, print new file
    print open(fileName).read()
else:
    print "File %r was not cleaned because it is missing!" % fileName



# End
