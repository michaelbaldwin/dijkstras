package com.michaelbaldwin.shortestpathsolver

import org.scalatest._
import collection.mutable._

/**
 * Test suite to run performance tests on implementation of Dijkstra's
 * algorithm.
 * Utilizes ScalaTest framework.
 * Reference: http://doc.scalatest.org/2.2.4/index.html#org.scalatest.FunSuite
 */
class PerformanceSuite extends FunSuite with BeforeAndAfter {

  // initialize shortestpathsolver object to default
  var d : ShortestPathSolver = _

  // create string variable for files directory
  val strDir = "src/test/resources/performance/"

  // create string variables for Princeton test files
  // source: https://algs4.cs.princeton.edu/44sp/
  val file8 = "tinyEWD.txt"
  val file250 = "mediumEWD.txt"
  val file1_000 = "1000EWG.txt"
  val file10_000 = "10000EWG.txt"
  val file1_000_000 = "largeEWG.txt"

  // runs before each test
  before {

    // create new shortestpathsolver object
    d = new ShortestPathSolver
  }

  ignore("ShortestPathSolver should handle 8 nodes and 15 edge weights") {
    d.fileName = strDir + file8
    val resultA = d.shortestPath("7", "5", d.readFile)
    assert(resultA("distance") === "0.28")
    assert(resultA("path") === "7 => 5")
    val resultB = d.shortestPath("1", "6", d.readFile)
    assert(resultB("distance") === "0.81")
    assert(resultB("path") === "1 => 3 => 6")
    val resultC = d.shortestPath("0", "2", d.readFile)
    assert(resultC("distance") === "0.26")
    assert(resultC("path") === "0 => 2")
  }

  ignore("ShortestPathSolver should handle 250 nodes and 2,546 edge weights") {
    d.fileName = strDir + file250
    var result = d.shortestPath("3", "211", d.readFile)
    assert(result("distance") === "0.547")
    assert(result("path") === "3 => 45 => 104 => 248 => 97 => 0 => 211")
  }

  ignore("ShortestPathSolver should handle 1,000 nodes and 16,866 edge weights") {
    d.fileName = strDir + file1_000
    var result = d.shortestPath("7", "511", d.readFile)
    assert(result("distance") === "0.36673999999999995")
    assert(result("path") === "7 => 68 => 166 => 259 => 26 => 365 => 112 => 511")
  }

  ignore("ShortestPathSolver should handle 10,000 nodes and 123,462 edge weights") {
    d.fileName = strDir + file10_000
    var result = d.shortestPath("22", "6870", d.readFile)
    println(file10_000 + result)
  }

  ignore("ShortestPathSolver should handle 1,000,000 nodes and 15,172,126 edge weights") {
    d.fileName = strDir + file1_000_000
    var result = d.shortestPath("1", "999999", d.readFile)
    println(file1_000_000 + result)
  }

}
