package com.michaelbaldwin.shortestpathsolver

import org.scalatest._
import collection.mutable._

/**
 * Test suite to test PriorityQueueDijkstra class.
 * Utilizes ScalaTest framework.
 * Reference: http://doc.scalatest.org/2.2.4/index.html#org.scalatest.FunSuite
 */
class PriorityQueueDijkstraSuite extends FunSuite with BeforeAndAfter {

  // create priority queue object to test, initialize it to default value
  var pq: PriorityQueueDijkstra = _

  // runs before each test (set up)
  before {
    pq = new PriorityQueueDijkstra
  }

  test("A new priority queue should be empty") {
    assert(pq.isEmpty === true)
  }

  test("A new priority queue should have size 0") {
    assert(pq.size === 0)
  }

  test("A priority queue with at least one item is not empty") {
    var nodeA = new Node("A")
    nodeA.distance = 1
    pq.insert(nodeA.name, nodeA)
    assert(pq.isEmpty === false)
  }

  test("A priority queue with one item has size 1") {
    var nodeA = new Node("A")
    nodeA.distance = 1
    pq.insert(nodeA.name, nodeA)
    assert(pq.size === 1)
  }

  test("A priority queue should handle inserts") {
    var nodeA = new Node("A")
    nodeA.distance = 1
    pq.insert(nodeA.name, nodeA)
    var nodeB = new Node("B")
    nodeB.distance = 2
    pq.insert(nodeB.name, nodeB)
    var nodeC = new Node("C")
    nodeC.distance = 3
    pq.insert(nodeC.name, nodeC)
    var nodeD = new Node("D")
    nodeD.distance = 1.5
    pq.insert(nodeD.name, nodeD)
    var nodeE = new Node("E")
    nodeE.distance = 2.5
    pq.insert(nodeE.name, nodeE)
    assert(pq.size === 5)
  }

  test("A priority queue should remove the minimum node") {
    var nodeA = new Node("A")
    nodeA.distance = 1
    pq.insert(nodeA.name, nodeA)
    var nodeB = new Node("B")
    nodeB.distance = 2
    pq.insert(nodeB.name, nodeB)
    var nodeC = new Node("C")
    nodeC.distance = 3
    pq.insert(nodeC.name, nodeC)
    var nodeD = new Node("D")
    nodeD.distance = 1.5
    pq.insert(nodeD.name, nodeD)
    var nodeE = new Node("E")
    nodeE.distance = 2.5
    pq.insert(nodeE.name, nodeE)
    assert(pq.deleteMin === nodeA)
    assert(pq.deleteMin === nodeD)
    assert(pq.deleteMin === nodeB)
    assert(pq.deleteMin === nodeE)
    assert(pq.deleteMin === nodeC)
  }

  test("A priority queue should handle decrease key") {
    var nodeA = new Node("A")
    nodeA.distance = 1
    pq.insert(nodeA.name, nodeA)
    var nodeB = new Node("B")
    nodeB.distance = 2
    pq.insert(nodeB.name, nodeB)
    var nodeC = new Node("C")
    nodeC.distance = 3
    nodeB.distance = 0.5
    pq.decreaseKey(nodeB.name, nodeB)
    assert(pq.deleteMin === nodeB)
  }

}
