package com.michaelbaldwin.shortestpathsolver

import org.scalatest._
import collection.mutable._
import java.io.{IOException, FileNotFoundException}

/**
 * Test suite to test ShortestPathSolver class.
 * Utilizes ScalaTest framework.
 * Reference: http://doc.scalatest.org/2.2.4/index.html#org.scalatest.FunSuite
 */
class ShortestPathSolverSuite extends FunSuite with BeforeAndAfter {

  // initialize shortestpathsolver object to default
  var d : ShortestPathSolver = _

  // create string variable for files directory
  val strDir = "src/test/resources/correctness/"

  // create string variables for provided set of inputs
  val A = "A"
  val B = "B"
  val C = "C"
  val D = "D"

  // create string variable for arrow in path
  val arw = " => "

  // before each test
  before {

    // create new shortestpathsolver object
    d = new ShortestPathSolver
  }

  test("shortestpathsolver must read edges in from a file") {
    intercept[FileNotFoundException] {
      d.fileName = strDir + "MISSING.txt"
      d.readFile
    }
  }

  test("shortestpathsolver must detect empty lines when reading") {
    intercept[IllegalArgumentException] {
      d.fileName = strDir + "empty-line.txt"
      d.readFile
    }
  }

  test("shortestpathsolver must detect too little arguments") {
    intercept[IllegalArgumentException] {
      d.fileName = strDir + "too-little-arguments.txt"
      d.readFile
    }
  }

  test("shortestpathsolver must detect too many arguments") {
    intercept[IllegalArgumentException] {
      d.fileName = strDir + "too-many-arguments.txt"
      d.readFile
    }
  }

  test("shortestpathsolver must detect missing start node") {
    intercept[IllegalArgumentException] {
      d.fileName = strDir + "missing-start.txt"
      d.readFile
    }
  }

  test("shortestpathsolver must detect missing target node") {
    intercept[IllegalArgumentException] {
      d.fileName = strDir + "missing-target.txt"
      d.readFile
    }
  }

  test("shortestpathsolver must detect missing distance") {
    intercept[IllegalArgumentException] {
      d.fileName = strDir + "missing-distance.txt"
      d.readFile
    }
  }

  test("shortestpathsolver must detect not numeric distance") {
    intercept[IllegalArgumentException] {
      d.fileName = strDir + "not-numeric-distance.txt"
      d.readFile
    }
  }

  test("shortestpathsolver must detect negative distance") {
    intercept[IllegalArgumentException] {
      d.fileName = strDir + "negative-distance.txt"
      d.readFile
    }
  }

  test("shortestpathsolver should read in file properly") {
    d.fileName = strDir + "5-directed.txt"
    val result = d.readFile
    var buffer = new ListBuffer[Map[String,Any]]
    buffer += Map("startLocation" -> "0", "endLocation" -> "1", "distance" -> 2.0)
    buffer += Map("startLocation" -> "1", "endLocation" -> "0", "distance" -> 2.0)
    buffer += Map("startLocation" -> "0", "endLocation" -> "3", "distance" -> 4.0)
    buffer += Map("startLocation" -> "1", "endLocation" -> "3", "distance" -> 1.0)
    buffer += Map("startLocation" -> "1", "endLocation" -> "2", "distance" -> 3.5)
    buffer += Map("startLocation" -> "2", "endLocation" -> "1", "distance" -> 3.5)
    buffer += Map("startLocation" -> "4", "endLocation" -> "3", "distance" -> 5.0)
    buffer += Map("startLocation" -> "4", "endLocation" -> "1", "distance" -> 1.5)
    buffer += Map("startLocation" -> "4", "endLocation" -> "2", "distance" -> 0.5)
    val expectedList = buffer.toList
    assert(result === expectedList)
  }

  test("shortestpathsolver should find shortest path from start to target") {
    d.fileName = strDir + "5-directed.txt"
    val result = d.shortestPath("0", "2", d.readFile)
    assert(result("distance") === "5.5")
    assert(result("path") === "0 => 1 => 2")
  }

  test("shortestpathsolver should find shortest path from " + A + " to " + D) {
    d.fileName = strDir + "dijkstras-directed.txt"
    val result = d.shortestPath(A, D, d.readFile)
    assert(result("distance") === "6.0")
    val path = A + arw + B + arw + C + arw + D
    assert(result("path") === path)
  }

  test("shortestpathsolver should find shortest path from " + B + " to " + D) {
    d.fileName = strDir + "dijkstras-directed.txt"
    val result = d.shortestPath(B, D, d.readFile)
    assert(result("distance") === "5.0")
    val path = B + arw + C + arw + D
    assert(result("path") === path)
  }
}
