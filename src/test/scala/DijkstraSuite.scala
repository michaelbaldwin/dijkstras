package com.michaelbaldwin.shortestpathsolver

import org.scalatest._
import collection.mutable._

/**
 * Test suite to test Dijkstra class.
 * Utilizes ScalaTest framework.
 * Reference: http://doc.scalatest.org/2.2.4/index.html#org.scalatest.FunSuite
 */
class DijkstraSuite extends FunSuite with BeforeAndAfter {

  // create a dijkstra object to test, initialize to default
  var d: Dijkstra = _

  // runs before each test
  before {

    // create shortestpathsolver object
    var s = new ShortestPathSolver

    // assign file name to shortestpathsolver object
    s.fileName = "src/test/resources/correctness/5-directed.txt"

    // read in edges data from file
    s.readFile

    // create dijkstra object using nodes and edges from shortestpathsolver object
    d = new Dijkstra(s.nodes, s.edges)
  }

  test("Start node must be in the graph") {
    intercept[IllegalArgumentException] {
      d.shortestPath("MISSING", "0")
    }
  }

  test("Target node must be in the graph") {
    intercept[IllegalArgumentException] {
      d.shortestPath("0", "MISSING")
    }
  }

  test("Target node must be reachable from start node") {
    intercept[IllegalArgumentException] {
      d.shortestPath("0", "4")
    }
  }

  test("Dijkstra's algorithm should find path from start to target") {
    val result = d.shortestPath("4", "2")
    assert(result("distance") === "0.5")
    assert(result("path") === "4 => 2")
  }

  test("Dijkstra's algorithm should find shortest path from start to target") {
    val result = d.shortestPath("0", "3")
    assert(result("distance") === "3.0")
    assert(result("path") === "0 => 1 => 3")
  }

}
