package com.michaelbaldwin.shortestpathsolver

import org.scalatest._
import collection.mutable._

/**
 * Test suite to test Node class.
 * Utilizes ScalaTest framework.
 * Reference: http://doc.scalatest.org/2.2.4/index.html#org.scalatest.FunSuite
 */
class NodeSuite extends FunSuite with BeforeAndAfter {

  // create a node object to test
  var node: Node = _

  // runs before each test
  before {

    // create a new node
    node = new Node("test")
  }

  test("A new node should have an identifying name") {
    assert(node.name === "test")
  }

  test("A new node should be unknown") {
    assert(node.known === false)
  }

  test("A new node should have infinity distance") {
    assert(node.distance === Double.MaxValue)
  }

  test("A new node should have no neighbors") {
    assert(node.neighbors === List())
  }

  test("A new node should have no previous node for path") {
    assert(node.previous === None)
  }

  test("A node should be able to be known") {
    node.known = true
    assert(node.known === true)
  }

  test("A node's distance should be mutable") {
    node.distance = 2.0
    assert(node.distance === 2.0)
  }

  test("A node should be able to have neighbors") {
    var neighborsBuffer = new ListBuffer[Node]
    val nodeA = new Node("A")
    neighborsBuffer += nodeA
    val nodeB = new Node("B")
    neighborsBuffer += nodeB
    node.neighbors = neighborsBuffer.toList
    assert(node.neighbors.size === 2)
  }

  test("A node should be able to have a previous node as path") {
    val nodePrev = new Node("previous")
    node.previous = Some(nodePrev)
    assert((node.previous).get === nodePrev)
  }

}
