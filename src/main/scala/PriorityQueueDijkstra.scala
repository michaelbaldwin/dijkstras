/**
 * @project Shortest Path Solver
 * @author Michael Baldwin
 * @date 06/06/2015
 * @usage Scala implementation of indexed min-heap priority queue for Dijkstra's
 * algorithm.
 * @description
 *
 * Implements a binary min-heap using a zero based array buffer where indexes
 * hold nodes in the graph and a map where a node name maps to the index in the
 * array buffer where that node is located in the min-heap.
 *
 * Min-heap is ordered based on the distance value in nodes. Data structure is
 * optimized for Dijkstra's algorithm, not only with deleteMin function but
 * especially with implementation of decrease key function, which performs a
 * constant time lookup of where the node is located in heap, updates the
 * node's value, and then re-orders the priority queue.
 *
 * @references
 *    * Problem Solving with Algorithms and Data Structures by Brad Miller and
 *      David Ranum - Binary Heap Implementation
 */
package com.michaelbaldwin.shortestpathsolver

import scala.collection.mutable._

/**
 * PriorityQueueDijkstra class, which implements a priority queue by using an
 * indexed binary min-heap of nodes.
 * @param nodes ArrayBuffer of nodes ordered by smallest distance first
 * @param indexes Map where key is node name and value is array buffer index
 */
class PriorityQueueDijkstra(var nodes: ArrayBuffer[Node] = ArrayBuffer(),
                            var indexes: Map[String,Int] = Map()) {

  /**
   * Checks whether the priority queue is empty or not.
   * @return Boolean representing if priority queue is empty or not
   */
  def isEmpty() = { nodes.isEmpty }

  /**
   * Gets the size of the priority queue.
   * @return Int number of elements currently in the priority queue
   */
  def size() = { nodes.size }

  /**
   * Given the index of a node in the min-heap, gets the index of that node's
   * parent.
   * @return Int index of the parent to a node in the min-heap
   */
  private def parentIndex(index: Int) = { (index - 1) / 2 }

  /**
   * Given the index of a node in the min-heap, gets the index of that node's
   * left child.
   * @return Int index of the left child to a node in the min-heap
   */
  private def leftChildIndex(index: Int) = { (2 * index) + 1 }

  /**
   * Given the index of a node in the min-heap, gets the index of that node's
   * right child.
   * @return Int index of the right child to a node in the min-heap
   */
  private def rightChildIndex(index: Int) = { (2 * index) + 2 }

  /**
   * Get the index of the first node in the min-heap.
   * @return Int index of first node
   */
  private def firstIndex() = { 0 }

  /**
   * Get the index of the last node in the min-heap.
   * @return Int index of last node
   */
  private def lastIndex() = { nodes.size - 1 }

  /**
   * Insert value into priority queue using key to map to its location in the
   * min-heap.
   * @param key String name of node
   * @param value Node to be inserted into priority queue
   */
  def insert(key: String, value: Node) = {

    // add value to array buffer
    nodes += value

    // map the key to last index of array buffer, where value just inserted
    indexes(key) = lastIndex()

    // ensure heap ordering starting from bottom of min-heap
    percolateUp(lastIndex(), value)
  }

  /**
   * Given the last index where a node just inserted, check to make sure the
   * priority queue is properly ordered.
   * @param lastIndex where node just inserted at bottom of min-heap.
   * @param value Node inserted into min-heap.
   */
  private def percolateUp(lastIndex: Int, value: Node) = {

    // only percolate up if at least two items in priority queue
    if (size() > 1) {

      // create index variable using parameter
      var i = lastIndex

      // while index is still before root (at least two values to compare), and
      // parent distance is greater than value (swap necessary)
      while (i > 0 & value.distance < nodes(parentIndex(i)).distance) {

        // if index node's distance is less than its parent node's distance,
        // then swap the child with the parent
        if (nodes(i).distance < nodes(parentIndex(i)).distance)
          swap(nodes(i), nodes(parentIndex(i)))

        // update index to be that of the parent
        i = parentIndex(i)
      }
    }
  }

  /**
   * Swap two nodes in the priority queue.
   * @param nodeX first node to be swapped with second.
   * @param nodeY second node to swapped with first.
   */
  private def swap(nodeX: Node, nodeY: Node) {

    // swap nodes in the array buffer
    val tempNode = nodes(indexes(nodeY.name))
    nodes(indexes(nodeY.name)) = nodes(indexes(nodeX.name))
    nodes(indexes(nodeX.name)) = tempNode

    // update map index values
    val tempIndex = indexes(nodeY.name)
    indexes(nodeY.name) = indexes(nodeX.name)
    indexes(nodeX.name) = tempIndex
  }

  /**
   * Delete the smallest element from the priority queue and return it.
   * @return Node at front of priority queue.
   * @throws IndexOutOfBoundsException if nothing in priority queue.
   */
  def deleteMin() = {

    // if priority queue is empty, throw an exception
    if (isEmpty())
      throw new IndexOutOfBoundsException(Console.RED + "Error: " +
        Console.RESET + " priority queue must not be empty to remove smallest" +
      " element.")

    // get the first node in the priority queue
    val minNode = nodes(firstIndex())

    // if at least two items in priority queue, swap first node with last node
    if (size() > 1)
      swap(nodes(firstIndex()), nodes(lastIndex()))

    // remove the last node from the array buffer
    nodes.remove(lastIndex())

    // remove node name key from indexes map
    indexes.remove(minNode.name)

    // if at least two items in priority queue, ensure heap ordering starting
    // from top of min-heap
    if (size() > 1)
      percolateDown(firstIndex(), nodes(firstIndex()))

    // return node with the minimum distance
    minNode
  }

  /**
   * Given the first index where a node just swapped, check to make sure the
   * priority queue is properly ordered.
   * @param firstIndex Index of node just swapped here.
   * @param value Node swapped to first index in min-heap.
   */
  private def percolateDown(firstIndex: Int, value: Node) = {

    // only percolate down if priority queue has at least two items
    if (size() > 1) {

      // create index variable starting at top of min-heap
      var i = firstIndex

      // while index is before last element, and when the value is greater
      // than the minimum child node distance (node at i can be swapped down)
      while (i < lastIndex() & value.distance > minChild(i).distance) {

        // get the smaller child based on index in min-heap
        val minNode = minChild(i)

        // get the index of this minimum child
        val minIndex = indexes(minNode.name)

        // if node at index is greater than minimum child, swap the two nodes
        if (nodes(i).distance > minNode.distance)
          swap(nodes(i), nodes(minIndex))

        // update index to be that of minimum child index
        i = minIndex
      }
    }
  }

  /**
   * Finds and returns the smaller child of a node at given index in min-heap.
   * If there is no right child, returns the left child node.
   * @param Int index where node located
   * @return Node which is smaller child of node at given index
   */
  private def minChild(index: Int) = {

    // create minimum node to hold smaller child and set to default value
    var minNode = new Node("")
    minNode.distance = Double.MaxValue

    // if children are within min-heap
    if (leftChildIndex(index) <= size() & rightChildIndex(index) <= size()) {

      // if right child index is equal to size of array buffer then there is no
      // right child so return left child, else return smaller of two children
      if (rightChildIndex(index) == size())
        minNode = nodes(leftChildIndex(index))
      else {

        // compare left and right children, return smaller of the two
        if (nodes(leftChildIndex(index)).distance <
            nodes(rightChildIndex(index)).distance)
          minNode = nodes(leftChildIndex(index))
        else
          minNode = nodes(rightChildIndex(index))
      }
    }

    // return the minimum child node
    minNode
  }

  /**
   * Reduce value at specific position by a specific amount, using the key of
   * node name to map to position index in array buffer representing min-heap.
   * @param String key mapping to index in min-heap.
   * @param Node updated node with smaller distance value.
   */
  def decreaseKey(key: String, value: Node) = {

    // get index of value using node name
    val i = indexes(value.name)

    // replace the node at that index with the updated node
    nodes(i) = value

    // ensure ordering with percolateUp from position just updated value
    percolateUp(i, value)
  }

}
