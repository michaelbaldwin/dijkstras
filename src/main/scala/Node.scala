/**
 * @project Shortest Path Solver
 * @author Michael Baldwin
 * @date 06/06/2015
 * @description
 *
 * A node is the unit from which a graph is formed and in this case represents
 * a location.
 *
 * The node's name identifies it from the other nodes in the graph, and each
 * node contains a list of neighbors, or the names of those nodes which can be
 * reached from said node.
 *
 * In terms of Dijkstra's algorithm, a node is either known or not known as it
 * becomes discovered (or not discovered) by the running algorithm, and holds a
 * tentative distance to the target node, and a pointer to the node the
 * algorithm was previously at before moving to said node.
 *
 * @usage Node class is used in Dijkstra's algorithm.
 */
package com.michaelbaldwin.shortestpathsolver

import scala.collection.mutable._

/**
 * Node class representing a vertex in the graph.
 * @param name Name identifying the node.
 * @param known Whether the node is known or not in Dijkstra's algorithm.
 * @param distance Tentative distance to target node in Dijkstra's algorithm.
 * @param neighbors List of neighbor nodes to this node.
 * @param previous Node previously moved from as part of path trail.
 */
class Node(val name: String, var known: Boolean = false,
           var distance: Double = Double.MaxValue,
           var neighbors: List[Node] = List(),
           var previous: Option[Node] = None) {

  /**
   * Override toString method to print state.
   * @return String representing state of this node in Dijkstra's algorithm.
   */
  override def toString() = "[name=" + name + ", known=" + known +
    ", distance=" + distance + ", neighbors=" + getNeighborNames(neighbors) +
    ", previous=" + previous + "]"

  /**
   * Get just the names of neighbor nodes to avoid polluting the console with
   * unncecessary information about each neighbor node object.
   * @param neighbors list of neighbor nodes
   */
  private def getNeighborNames(neighbors: List[Node]) = {
      // if edge weight is negative, throw illegal argument exception because
      // dijkstra's algorithm assumes non-negative edge weights.

    // create string result and denote as list
    var result : String = "List("

    // create counter to track if before end of list
    var count = 0;

    // loop through list of neighbors
    for (node <- neighbors) {

      // add name to string result
      result += node.name

      // if list element is not last add comma
      if (count != neighbors.length - 1)
        result += ", "

      // increment count
      count += 1
    }

    // close list
    result += ")"

    // return string
    result
  }

}
