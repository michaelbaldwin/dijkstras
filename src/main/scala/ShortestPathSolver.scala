/**
 * @project Shortest Path Solver
 * @author Michael Baldwin
 * @date 06/06/2015
 * @usage Reads in graph from file and runs Dijkstra's algorithm to find
 * shortest path between a starting location and a target location.
 * @description
 *
 * Each line in the file represents an edge with direction.
 * The data is in CSV ("Comma Separated Values") file format, where the three
 * columns are startNode, endNode, edgeWeight (respectively).
 *
 * startNode, endNode, edgeWeight
 * A, B, 9      // node A can reach node B via edge A->B with weight of 9
 *
 * Since an undirected graph can be more easily converted to a directed graph
 * than the other way around, each edge is assumed to represent an edge in a
 * directed graph to ensure the program handle both types of graphs.
 *
 * A, B, 9      // node A can reach node B
 * B, A, 9      // node B can reach node A
 * A, C, 3      // node A can reach node C, but node C can't reach node A
 *
 * Note how an edge (A->B) in an undirected graph can be represented by
 * converting that edge to two edges (A->B, B->A) in a directed graph.
 *
 * The file data must be represented properly in order for the program to build
 * the graph correctly.
 *
 * The shortest path function attempts to find a shortest path between a source
 * node and target node by traversing the nodes in the graph and building a
 * path where the edge weight is minimized for using Dijkstra's algorithm.
 *
 * @references
 *    * Scala for the Impatient by Cay S. Horstmann
 *    * Algorithms, 4th Edition by Robert Sedgewick and Kevin Wayne - Shortest
 *      Paths
 * @dependencies Node, Dijkstra
 */
package com.michaelbaldwin.shortestpathsolver

import java.io.{IOException, FileNotFoundException}
import scala.collection.mutable._
import scala.io.Source._

/**
 * ShortestPathSolver class
 * Reads in list of edges from text file and builds graph of vertices and edges
 * from input data. Shortest path function uses Dijkstra's algorithm implemented
 * in Dijkstra class.
 * @param fileName name of file that holds list of edges
 * @param nodeNames set of node names (ignores duplicates)
 * @param nodes map of nodes where key is node name and value is node
 * @param edges list of maps representing edges between nodes
 */
class ShortestPathSolver(var fileName: String = "",
                   var nodeNames: Set[String] = Set(),
                   var nodes: Map[String,Node] = Map(),
                   var edges: List[Map[String,Any]] = List()) {

  /**
   * Read list of edges from file into list of maps representing edges
   * @return List[Map[String,Any]] list of maps representing edges
   */
  def readFile = {

    // source object, initialization deferred until accessed for first time
    lazy val file = fromFile(fileName)

    // create mutable list buffer to hold edges
    var edgesBuffer = new ListBuffer[Map[String, Any]]

    // try to read file
    try {

      // initialize counter for error messages at 1
      var count = 1

      // loop through each line in file
      for (line <- file.getLines()) {

        // process each line and add the returned map onto list buffer
        edgesBuffer += processLine(line, count)

        // increment counter
        count = count + 1
      }

      // convert list buffer to list and set the edges
      edges = edgesBuffer.toList

      // create node objects using now populated set of nodes
      createNodes()

      // for each node, set a list of neighbor nodes using edge data
      setNeighbors()
    }
    finally {

      // release resources back to operating system
      file.close()
    }

    // return list of maps representing edges
    edges
  }

  /**
   * Read line of data from file into map and return map
   * @param fileLine line of data representing an edge
   * @param lineNumber integer representing which line number at in file
   * @return Map[String,Any] map representing an edge
   * @throws IllegalArugmentException if line is empty
   * @throws IllegalArgumentException if line does not have exactly three
   * comma separated values as arguments
   * @throws IllegalArgumentException if start node is missing
   * @throws IllegalArgumentException if target node is missing
   * @throws IllegalArgumentException if distance is missing
   * @throws IllegalArgumentException if distance is not numeric
   * @throws IllegalARgumentException if distance is negative
   */
  private def processLine(fileLine: String, lineNumber: Int) = {

    // if line is empty, throw illegal argument exception
    if (fileLine.isEmpty)
      throw new IllegalArgumentException(Console.RED + "Error: " +
        Console.RESET + "Line " + lineNumber + " must not be empty: " +
        fileLine)

    // split the file line's string into an array of strings
    val strArray = fileLine.split(",")

    // if array is not length of 3, throw illegal argument exception
    if (strArray.length != 3)
      throw new IllegalArgumentException(Console.RED + "Error: " +
        Console.RESET + "Line " + lineNumber + " must have exactly three " +
        "comma separated values: " + fileLine)

    // parse in the start node name, target node name, and edge weight distance
    val start = strArray(0).trim
    val target = strArray(1).trim
    val distanceStr = strArray(2).trim

    // if any strings are empty, throw illegal argument exception
    if (start.isEmpty)
      throw new IllegalArgumentException(Console.RED + "Error: " +
        Console.RESET + "Line " + lineNumber + " must have a start node: " +
        fileLine)
    if (target.isEmpty)
      throw new IllegalArgumentException(Console.RED + "Error: " +
        Console.RESET + "Line " + lineNumber + " must have a target node: " +
        fileLine)
    if (distanceStr.isEmpty)
      throw new IllegalArgumentException(Console.RED + "Error: " +
        Console.RESET + "Line " + lineNumber + " must have an edge weight: " +
        fileLine)

    // try to convert distance string to numeric double
    val distance = parseDouble(distanceStr)

    // use pattern matching to determine whether string was numeric
    distance match {
      case Some(x) => // do nothing because value was parsed correctly
      case None => throw new IllegalArgumentException(Console.RED + "Error: " +
        Console.RESET + "Line " + lineNumber + " must have an edge weight " +
        "with a numeric value: " + fileLine)
    }

      // if edge weight is negative, throw illegal argument exception because
    // dijkstra's algorithm assumes non-negative edge weights.
    if (distance.get < 0)
      throw new IllegalArgumentException(Console.RED + "Error: " +
        Console.RESET + "Line " + lineNumber + " must have a non-negative " +
      "edge weight: " + fileLine)

    // concatenate current set and new set of node names from edge data
    // duplicate node names are avoided here by using a set data structure
    nodeNames = nodeNames ++ Set(start, target)

    // create and return map filled with line's data
    Map("startLocation" -> start, "endLocation" -> target,
        "distance" -> distance.get)
  }

  /*
   * Tries to convert a string to a numeric double. If the string is converted
   * successfully, wraps double into Some. If conversion is not possible,
   * returns None.
   * @param String to be converted to a double
   * @return Option double representing string converted to numeric
   */
  private def parseDouble(str: String) = {

    // try to convert string to a double, catch any exceptions
    try {

      // wrap the converted string in an Option object as Some(val)
      Some(str.toDouble)
    }
    catch {

      // any exceptions, return None
      case ex: Exception => None
    }
  }

  /**
   * Create nodes in graph using set of node names.
   */
  private def createNodes() = {

    // loop through the set of node names
    // create node object and add to map of nodes using name as key
    for (name <- nodeNames)
      nodes(name) = new Node(name)
  }

  /**
   * Set each node's list of neighbors using edge data.
   */
  private def setNeighbors() = {

    // now that all nodes created, loop through nodes
    // find neighbors of that node using node name and edge data
    for (name <- nodeNames)
      nodes(name).neighbors = findNeighbors(name)
  }

  /**
   * Find the neighbor nodes of a particular node using the node's name and
   * edge data.
   * @param nodeName name of the node
   * @return List[Node] list of nodes representing neighbors of a node
   */
  private def findNeighbors(nodeName: String) = {

    // create list buffer to hold neighbor nodes
    var neighborsBuffer = new ListBuffer[Node]

    // loop through list of edges
    for (edge <- edges) {

      // if "startLocation" is node name, add "endLocation" node as neighbor
      if (edge("startLocation").equals(nodeName))
        neighborsBuffer += nodes(edge("endLocation").toString)
    }

    // convert list buffer to list and return list of neighbors
    neighborsBuffer.toList
  }

  /**
   * Find shortest path from starting location to target location when given
   * list of edges with no negative weights in a directed graph
   * @param startingLocation start node
   * @param targetLocation end node
   * @param edges list of maps representing edges
   * @return Map[String,String] map representing result path and distance
   */
  def shortestPath(startingLocation: String, targetLocation: String,
                   edges: List[Map[String,Any]]) = {

    // create new dijkstra object from nodes and edges
    val d = new Dijkstra(nodes, edges)

    // create path variable
    var path : Map[String,String] = Map()

    // return shortest path found in graph given start node and end node
    d.shortestPath(startingLocation, targetLocation)
  }

  /**
   * Print the given input to the console.
   * @param startingLocation name of start node
   * @param targetLocation name of end node
   */
  private def printInput(startingLocation : String, targetLocation : String) = {

    // print given name of start node
    println("startingLocation: " + startingLocation)
    println("")

    // print given name of end node
    println("targetLocation: " + targetLocation)
    println("")

    // print given file data of edges
    println("edges: ")
    println("")
    println("List(")
    for (map <- edges) {
      println("  " + map + ",")
    }
    println(")")
    println("")
  }

}

/**
 * Runs program. Using object keyword follows singleton design pattern.
 * Extending App trait removes need to provide main method.
 */
object ShortestPathSolver extends App {

  // try to read in file and find shortest path, catch exceptions
  try {

    // create immutable string for file location
    val fileName = "src/main/resources/dijkstras-directed.txt"

    // create shortest path solver object
    var d = new ShortestPathSolver(fileName)

    // create names of start and target locations
    val startingLocation = "A"
    val targetLocation = "D"

    // find shortest path from start to end
    var result = d.shortestPath(startingLocation, targetLocation, d.readFile)

    // print input data to screen
    d.printInput(startingLocation, targetLocation)

    // print result
    println("result: " + result)
  }
  catch {
      case ex: IllegalArgumentException => println(ex.getMessage())
      case ex: FileNotFoundException => println(ex.getMessage() + "\n" +
                                                ex.printStackTrace())
      case ex: IOException => println(ex.getMessage() + "\n" +
                                      ex.printStackTrace())
      case ex: Exception => println(ex.getMessage() + "\n" +
                                    ex.printStackTrace())
  }

}
