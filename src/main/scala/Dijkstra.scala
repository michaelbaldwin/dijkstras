/**
 * @project Shortest Path Solver
 * @author Michael Baldwin
 * @date 06/06/2015
 * @usage Scala implementation of Dijkstra's algorithm using priority queue.
 * @description
 *
 * Shortest path function utilizing Dijkstra's algorithm. Note that Dijkstra's
 * algorithm assumes using edges with non-negative edge weights.
 *
 * Starting at a source node, attempts to find the shortest path to a target
 * node in a graph of nodes using edge weights between nodes in the graph as the
 * minimizing factor.
 *
 * @references
 *    * Data Structures and Algorithm Analysis in C++ by Mark Allen Weiss -
 *      Shortest-Path Algorithms
 * @dependencies Node, PriorityQueueDijkstra
 */
package com.michaelbaldwin.shortestpathsolver

import scala.collection.mutable._

/**
 * Dijkstra class, which implements a shortest path function from start node to
 * target node using Dijkstra's algorithm.
 * @param nodes Map of nodes with key as name and value as Node
 * @param edges List of maps representing edges
 */
class Dijkstra(val nodes: Map[String,Node], val edges: List[Map[String,Any]]) {

  /**
   * Shortest path function from start node to target node using Dijkstra's
   * algorithm.
   * @param startingLocation Name of start node
   * @param targetLocation Name of end node
   * @return listOfEdges Each edge represented as map and connects two locations
   */
  def shortestPath(startingLocation: String, targetLocation: String) = {

    // verify both start and target nodes exist in graph before starting
    checkNodesExist(startingLocation, targetLocation)

    // create current node object
    var current : Option[Node] = None

    // create empty priority queue
    var minHeap = new PriorityQueueDijkstra

    // populate priority queue with nodes and set start node's distance to zero
    populatePriorityQueue(minHeap, startingLocation)

    // while there is a node with an unknown distance
    while (!minHeap.isEmpty) {

      // set current node to unknown node with smallest tentative distance
      current = Some(minHeap.deleteMin())

      // set current node as now known
      current.get.known = true

      // loop through neighbors of current node
      for (neighbor <- current.get.neighbors) {

        // if neighbor is not known
        if (!neighbor.known) {

          // get edge weight from current node to neighbor node
          val neighborCost = findEdgeWeight(current.get, neighbor)

          // if current distance plus neighbor cost is less than neighbor
          // distance, update neighbor node state
          if (current.get.distance + neighborCost < neighbor.distance) {

            // update neighbor distance to be current distance plus edge cost
            neighbor.distance = current.get.distance + neighborCost

            // update neighbor node in priority queue with new distance
            minHeap.decreaseKey(neighbor.name, neighbor)

            // add current node to path of neighbor node
            neighbor.previous = current
          }
        }
      }
    }

    // check if path from start node to target node found in graph
    checkPathFound(startingLocation, targetLocation)

    // return path represented as map
    createPath(startingLocation, targetLocation)
  }

  /**
   * Check to see if both start and target nodes exist in graph before running
   * algorithm.
   * @param startingLocation start node
   * @param targetLocation target node
   * @throws IllegalArgumentException if starting location does not exist in the
   * graph.
   * @throws IllegalArgumentException if target location does not exist in the
   * graph.
   */
  private def checkNodesExist(startingLocation : String,
                      targetLocation : String) = {

    // attempt to find both start and target node names in map of nodes
    val startNode = nodes.keySet.exists(_ == startingLocation)
    val targetNode = nodes.keySet.exists(_ == targetLocation)

    // if either node not found, thrown an illegal argument exception
    if (!startNode) {
      throw new IllegalArgumentException(Console.RED + "Error: " +
        Console.RESET + startingLocation + " does not exist in the graph")
    }
    if (!targetNode) {
      throw new IllegalArgumentException(Console.RED + "Error: " +
        Console.RESET + targetLocation + " does not exist in the graph")
    }
  }

  /**
   * Adds nodes to priority queue. Sets the tentative distance of the
   * start node to zero.
   * @param minHeap priority queue of unknown nodes
   * @param startingLocation start node
   */
  private def populatePriorityQueue(minHeap : PriorityQueueDijkstra,
                                    startingLocation : String) = {

    // loop through map of nodes, getting just values (node objects)
    for (node <- nodes.values) {

      // if node name is starting location, set distance to zero
      if (node.name.equals(startingLocation))
        node.distance = 0.0

      // add each node to priority queue of unknown nodes
      minHeap.insert(node.name, node)
    }

  }

  /**
   * Check to see if found path from start node to target node in graph.
   * @param startingLocation start node
   * @param targetLocation target node
   * @return Boolean represents whether path has been found.
   * @throws IllegalArgumentException if no path exists between starting
   * location and target location in the graph.
   */
  private def checkPathFound(startingLocation : String,
                             targetLocation : String)  = {

    // set path found to default of false
    var pathFound = false

    // if target node's distance less than infinity path was found, else throw
    // illegal argument exception
    if (nodes(targetLocation).distance < Double.MaxValue)
      pathFound = true
    else {
      throw new IllegalArgumentException(Console.RED + "Error: " +
        Console.RESET + "no path exists between " + startingLocation + " and " +
        targetLocation + " in the graph")
    }

    // return if path found
    pathFound
  }

  /**
   * Find edge weight between current node and neighbor node.
   * @param current node.
   * @param neighbor node.
   * @return Double edge weight.
   */
  private def findEdgeWeight(current: Node, neighbor: Node) = {

    // create weight variable with max value
    var weight = Double.MaxValue

    // loop through edges
    for (edge <- edges) {

      // if find edge between current node and neighbor node, return weight
      if (edge("startLocation").equals(current.name) &
          edge("endLocation").equals(neighbor.name))
        weight = edge("distance").toString.toDouble
    }

    // return weight
    weight
  }

  /**
   * Create path represented by a map.
   * @param startingLocation starting location name.
   * @param targetLocation target location name.
   * @return Map[String,String] representing path and distance from start to end
   */
  private def createPath(startingLocation: String, targetLocation: String) = {

    // create map and add node's distance
    var result = Map("distance" -> nodes(targetLocation).distance.toString)

    // get path trail and add to map
    result("path") = getPathTrail(nodes(targetLocation), startingLocation) +
                      " => " + targetLocation

    // return map
    result
  }

  /**
   * Recursively compute the path using previous nodes as trail.
   * @param node node at in graph.
   * @param location starting location name.
   * @return String concatenated path trail
   */
  private def getPathTrail(node: Node, location: String) : String = {

    // if node's previous node is starting location (base case), return it
    // else make recursive call and print previous node name after (reverses)
    if (node.previous.get.name.equals(location))
      return node.previous.get.name
    else
      return getPathTrail(node.previous.get, location) + " => " +
        node.previous.get.name
  }

}
